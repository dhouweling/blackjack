package com.infosupport.sample.blackjack;

public class HandResult {

	public enum Status {
		PLAYER_BLACKJACK, DEALER_WINS, PUSH, PLAYER_WINS
	}

	private final Hand hand;

	private final int amountWon;

	private final Status status;

	public HandResult(Hand hand, int amountWon, Status status) {
		this.hand = hand;
		this.amountWon = amountWon;
		this.status = status;
	}

	public Hand getHand() {
		return hand;
	}

	public int getAmountWon() {
		return amountWon;
	}

	public Status getStatus() {
		return status;
	}
}
