package com.infosupport.sample.blackjack;

import java.util.*;
import com.infosupport.sample.blackjack.card.*;

public class BlackjackMain {
	public static void main(String[] args) {
		Balance balance = new Balance(100);
		CardDealer dealer = new RealDealer();
		Game game = new Game(balance, dealer);

		for (int i = 0; i < 10; i++) {
			playGame(game);
			if (balance.get() == 0) {
				break;
			}
		}
		// TODO: save high score
	}

	private static void playGame(Game game) {
		game.start(10);
		Set<PlayerMove> movesAllowed = game.playerMovesAllowed();
		while (!movesAllowed.isEmpty()) {
			game.hit();
			movesAllowed = game.playerMovesAllowed();
		}
		for (HandResult result : game.finishGame()) {
			System.out.println("Result: " + result.getStatus());
		}
	}
}
