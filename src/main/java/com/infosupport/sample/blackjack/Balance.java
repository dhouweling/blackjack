package com.infosupport.sample.blackjack;

public class Balance {

	private int value;

	public Balance(int value) {
		this.value = value;
	}

	public int get() {
		return value;
	}

	public void add(int amount) {
		value += amount;
	}

	public void substract(int bet) {
		value -= bet;
	}
}
