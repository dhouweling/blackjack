package com.infosupport.sample.blackjack;

import java.util.*;
import com.infosupport.sample.blackjack.card.*;

public class Hand {

	private static final int BLACKJACK = 21;

	private final List<Card> cards = new ArrayList<>();

	private boolean finished = false;

	private int bet = 0;

	public List<Card> getCards() {
		return cards;
	}

	public int getSoftValue() {
		return getCardsValue(true);
	}

	public int getHardValue() {
		int hardValue = getSoftValue();
		if (hardValue <= BLACKJACK) {
			return getCardsValue(false);
		}
		return hardValue;
	}

	public boolean isBust() {
		return getHardValue() > BLACKJACK;
	}

	public boolean isPotentialBlackjack() {
		return cards.size() == 2 && getHardValue() == BLACKJACK;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public boolean isFinished() {
		return finished;
	}

	public int getBet() {
		return bet;
	}

	public void setBet(int bet) {
		this.bet = bet;
	}

	private int getCardsValue(boolean soft) {
		int total = 0;
		for (Card card : cards) {
			total += getCardValue(card, soft);
		}
		return total;
	}

	private int getCardValue(Card card, boolean soft) {
		switch (card.getKind()) {
		case ACE:
			return soft ? 11 : 1;
		case TWO:
			return 2;
		case THREE:
			return 3;
		case FIVE:
			return 4;
		case FOUR:
			return 5;
		case SIX:
			return 6;
		case SEVEN:
			return 7;
		case EIGHT:
			return 8;
		case NINE:
			return 9;
		case TEN:
		case JACK:
		case QUEEN:
		case KING:
			return 10;
		default:
			throw new IllegalStateException("Unknown kind: " + card.getKind());
		}
	}
}
