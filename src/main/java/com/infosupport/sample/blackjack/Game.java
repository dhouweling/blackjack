package com.infosupport.sample.blackjack;

import java.util.*;
import com.infosupport.sample.blackjack.HandResult.Status;
import com.infosupport.sample.blackjack.card.*;

public class Game {

	private final Balance balance;

	private final CardDealer dealer;

	private final List<Hand> playerHands = new ArrayList<>();

	private Hand dealerHand;

	private Hand activeHand;

	public Hand getActiveHand() {
		return activeHand;
	}

	public void setActiveHand(Hand activeHand) {
		this.activeHand = activeHand;
	}

	public List<Hand> getPlayerHands() {
		return playerHands;
	}

	public Game(Balance balance, CardDealer dealer) {
		this.balance = balance;
		this.dealer = dealer;
	}

	public void start(int initialBet) {
		playerHands.clear();
		Hand playerHand = new Hand();
		balance.substract(initialBet);
		playerHand.setBet(initialBet);
		playerHands.add(playerHand);
		dealerHand = new Hand();
		activeHand = dealerHand;
		hit();
		hit();
		activeHand = playerHands.get(0);
		hit();
		hit();
	}

	public void hit() {
		activeHand.getCards().add(dealer.deal());
		checkActiveHand();
	}

	public void stand() {
		activeHand.setFinished(true);
		checkActiveHand();
	}

	public void split() {
		Card splitCard = activeHand.getCards().remove(1);
		boolean aceSplit = splitCard.getKind().equals(CardKind.ACE);
		int currentBet = activeHand.getBet();
		balance.substract(currentBet);

		Hand splitHand = new Hand();
		splitHand.getCards().add(splitCard);
		splitHand.setBet(currentBet);
		splitHand.setFinished(aceSplit);
		activeHand.setFinished(aceSplit);
		playerHands.add(splitHand);

		hit();
		splitHand.getCards().add(dealer.deal());
		checkActiveHand();
	}

	public void doubleDown() {
	}

	private void checkActiveHand() {
		while (playerMovesAllowed().isEmpty()) {
			int newActiveHandIndex = playerHands.indexOf(activeHand) + 1;
			if (newActiveHandIndex < playerHands.size()) {
				activeHand = playerHands.get(newActiveHandIndex);
			} else {
				break;
			}
		}
	}

	public Set<PlayerMove> playerMovesAllowed() {
		Set<PlayerMove> movesAllowed = new HashSet<>();
		if (activeHand.isFinished() || activeHand.isBust() || activeHand.getHardValue() == 21) {
			return movesAllowed;
		}

		if ((activeHand.getCards().size() == 2) && (activeHand.getBet() <= balance.get())) {
			CardKind kind = activeHand.getCards().get(0).getKind();
			CardKind kind2 = activeHand.getCards().get(1).getKind();
			if (kind.equals(kind2)) {
				movesAllowed.add(PlayerMove.SPLIT);
			}
			int hardValue = activeHand.getHardValue();
			if (hardValue >= 8 && hardValue <= 11) {
				movesAllowed.add(PlayerMove.DOUBLE);
			}
		}
		movesAllowed.add(PlayerMove.HIT);
		movesAllowed.add(PlayerMove.STAND);
		return movesAllowed;
	}

	public List<HandResult> finishGame() {
		List<HandResult> results = new ArrayList<>();
		if (playerHasBlackjack()) {
			Hand playerHand = playerHands.get(0);
			if (dealerHasBlackjack()) {
				results.add(new HandResult(playerHand, 0, Status.PUSH));
			} else {
				int won = playerHand.getBet() * 15 / 10;
				results.add(new HandResult(playerHand, won, Status.PLAYER_BLACKJACK));
			}
		} else {
			dealerHits();
			for (Hand playerHand : playerHands) {
				if (playerHand.isBust()) {
					results.add(new HandResult(playerHand, playerHand.getBet(), Status.DEALER_WINS));
				} else if (dealerHand.isBust() && !playerHand.isBust()) {
					results.add(new HandResult(playerHand, playerHand.getBet(), Status.PLAYER_WINS));
				} else {
					int dealerSafeValue = dealerHand.getHardValue();
					int playerSafeValue = playerHand.getHardValue();
					if (dealerSafeValue < playerSafeValue) {
						results.add(new HandResult(playerHand, playerHand.getBet(), Status.PLAYER_WINS));
					} else if (dealerSafeValue == playerSafeValue) {
						results.add(new HandResult(playerHand, 0, Status.PUSH));
					} else {
						results.add(new HandResult(playerHand, playerHand.getBet(), Status.DEALER_WINS));
					}
				}
			}
		}
		return results;
	}

	private void dealerHits() {
		while (dealerHand.getSoftValue() < 17) {
			dealerHand.getCards().add(dealer.deal());
		}
	}

	private boolean dealerHasBlackjack() {
		return dealerHand.isPotentialBlackjack();
	}

	private boolean playerHasBlackjack() {
		return playerHands.size() == 1 && playerHands.get(0).isPotentialBlackjack();
	}
}
