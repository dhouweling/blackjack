package com.infosupport.sample.blackjack;

public enum PlayerMove {

	HIT, STAND, DOUBLE, SPLIT

}
