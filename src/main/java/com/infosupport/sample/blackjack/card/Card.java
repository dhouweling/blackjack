package com.infosupport.sample.blackjack.card;

public class Card {

	private final CardSuit suit;

	private final CardKind kind;

	public Card(CardSuit suit, CardKind kind) {
		this.suit = suit;
		this.kind = kind;
	}

	public CardKind getKind() {
		return kind;
	}

	@Override
	public String toString() {
		return suit + " " + kind;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kind == null) ? 0 : kind.hashCode());
		result = prime * result + ((suit == null) ? 0 : suit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Card other = (Card) obj;
		if (kind != other.kind) {
			return false;
		}
		if (suit != other.suit) {
			return false;
		}
		return true;
	}
}
