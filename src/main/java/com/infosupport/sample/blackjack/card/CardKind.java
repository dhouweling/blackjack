package com.infosupport.sample.blackjack.card;

public enum CardKind {

	ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING;
}
