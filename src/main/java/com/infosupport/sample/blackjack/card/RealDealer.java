package com.infosupport.sample.blackjack.card;

import java.util.*;

public class RealDealer implements CardDealer {

	private final Random random = new Random();

	@Override
	public Card deal() {
		int kindIndex = random.nextInt(CardKind.values().length);
		int suitIndex = random.nextInt(CardSuit.values().length);
		return new Card(CardSuit.values()[suitIndex], CardKind.values()[kindIndex]);
	}
}
