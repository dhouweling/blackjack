package com.infosupport.sample.blackjack.card;

public enum CardSuit {

	CLUBS, SPADES, HEARTS, DIAMONDS;
}
