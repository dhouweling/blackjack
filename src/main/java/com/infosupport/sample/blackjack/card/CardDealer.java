package com.infosupport.sample.blackjack.card;

public interface CardDealer {

	Card deal();
}
