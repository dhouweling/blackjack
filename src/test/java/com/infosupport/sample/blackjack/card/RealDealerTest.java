package com.infosupport.sample.blackjack.card;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.*;

public class RealDealerTest {

	private RealDealer dealer;

	@Before
	public void setUp() {
		dealer = new RealDealer();
	}

	@Test
	public void testTwoCardsAreDifferent() {
		Card card1 = dealer.deal();
		Card card2 = dealer.deal();
		assertThat(card1, is(not(card2)));
	}
}
