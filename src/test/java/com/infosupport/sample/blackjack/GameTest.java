package com.infosupport.sample.blackjack;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.*;
import com.infosupport.sample.blackjack.card.*;

public class GameTest {

	private static final class CardDealerMock implements CardDealer {

		private int nextCardIndex;
		private Card[] cardsToDeal;

		public void setCardsToDeal(Card... cards) {
			nextCardIndex = 0;
			cardsToDeal = cards;
		}

		@Override
		public Card deal() {
			return cardsToDeal[nextCardIndex++];
		}
	}

	private Game game;

	private CardDealerMock dealerMock;

	@Before
	public void setUp() {
		dealerMock = new CardDealerMock();
		game = new Game(new Balance(0), dealerMock);
	}

	@Test
	public void testHitsDrawsCard() {
		Card testCard = new Card(CardSuit.CLUBS, CardKind.EIGHT);
		dealerMock.setCardsToDeal(testCard);
		game.setActiveHand(new Hand());
		game.hit();
		//assertThat(game.getActiveHand().getCards(), hasItems(testCard));
	}
}
