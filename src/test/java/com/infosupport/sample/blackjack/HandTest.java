package com.infosupport.sample.blackjack;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.*;

import com.infosupport.sample.blackjack.card.*;

public class HandTest {

	private Hand hand;

	@Before
	public void setUp() {
		hand = new Hand();
	}

	@Test
	public void testSingleSixHasSoftHardValueSix() {
		addCard(CardKind.SIX);
		assertThat(hand.getSoftValue(), is(6));
		assertThat(hand.getHardValue(), is(6));
	}

	@Test
	public void testDoubleSevenHasSoftHardValueFourteen() {
		addCard(CardKind.SEVEN);
		addCard(CardKind.SEVEN);
		assertThat(hand.getSoftValue(), is(14));
		assertThat(hand.getHardValue(), is(14));
	}

	@Test
	public void testAceSevenHasSoftHardValueEighteen() {
		addCard(CardKind.ACE);
		addCard(CardKind.SEVEN);
		assertThat(hand.getSoftValue(), is(18));
		assertThat(hand.getHardValue(), is(8));
	}

	@Test
	public void testAceAceNineHasSoftHardValueTwentyOne() {
		addCard(CardKind.ACE);
		addCard(CardKind.ACE);
		addCard(CardKind.NINE);
		System.out.println(hand.getSoftValue());
		System.out.println(hand.getHardValue());
		//assertThat(hand.getSoftValue(), is(18));
		//assertThat(hand.getHardValue(), is(8));
	}

	private void addCard(CardKind kind) {
		hand.getCards().add(new Card(CardSuit.CLUBS, kind));
	}
}
