package com.infosupport.sample.blackjack;

import static com.infosupport.sample.blackjack.card.CardKind.*;
import static java.util.Arrays.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import java.util.*;
import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.Parameters;
import com.infosupport.sample.blackjack.card.*;

@RunWith(Parameterized.class)
public class HandValuesTest {

	@Parameters()
	public static Iterable<Object[]> data() {
		List<Object[]> result = new ArrayList<>();
		result.add(build(asList(SEVEN, SEVEN), 14, 14));
		result.add(build(asList(QUEEN, TEN), 20, 20));
		result.add(build(asList(ACE, SEVEN), 18, 8));
		result.add(build(asList(ACE, JACK), 21, 11));
		result.add(build(asList(SEVEN, SIX, TWO), 15, 15));
		result.add(build(Collections.<CardKind> emptyList(), 0, 0));
		return result;
	}

	private static Object[] build(List<CardKind> kinds, int expectedSoftValue, int expectedHardValue) {
		return new Object[] { kinds, expectedSoftValue, expectedHardValue };
	}

	private final List<CardKind> kinds;

	private final int expectedSoftValue;

	private final int expectedHardValue;

	private Hand hand;

	public HandValuesTest(List<CardKind> kinds, int expectedSoftValue, int expectedHardValue) {
		this.kinds = kinds;
		this.expectedSoftValue = expectedSoftValue;
		this.expectedHardValue = expectedHardValue;
	}

	@Before
	public void setUp() {
		hand = new Hand();
	}

	@Test
	public void test() {
		for (CardKind kind : kinds) {
			hand.getCards().add(new Card(CardSuit.CLUBS, kind));
		}
		assertThat(hand.getSoftValue(), is(expectedSoftValue));
		assertThat(hand.getHardValue(), is(expectedHardValue));
	}
}
